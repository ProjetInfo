#ifndef VECTOR_H
#define VECTOR_H

#include <iostream>
#include <stdexcept>
#include <exception>
#include <vector>
#include <cmath>

class Vector
{
    public:
        enum STANDARD_BASIS {ei};   //Allows instantiation of particular vectors of Rn

        //Constructors and destructors
        Vector ();
        Vector (const unsigned int dimension);
        Vector (const double cmp1, const double cmp2, const double cmp3);
        Vector (double components[], const unsigned int size);
        Vector (std::vector<double> components);
        /*Can throw logic_error exceptions
            /!\ Be aware that the index 'i' is understood as a mathematical index, not a general C++ index /!\*/
        Vector (const unsigned int dimension, STANDARD_BASIS aVector, const unsigned int i);
        Vector (const Vector &otherVector);

        //Accessors and modifiers
        const std::vector<double>& components () const {return m_components;}
        void setComponents (const std::vector<double> &newVector) {m_components = newVector;}
        unsigned int dimension () const;


        /*Operators
            All operators can throw logic_error exceptions*/
        void operator = (const Vector &otherVector);

        Vector operator + (const Vector &otherVector) const;
        Vector& operator += (const Vector &otherVector);

        Vector operator - (const Vector &otherVector) const;
        Vector& operator -= (const Vector &otherVector);

        Vector operator * (const double scalar) const;
        Vector& operator *= (const double scalar);
        double operator * (const Vector &otherVector) const;        //Dot product of two vectors

        //Can throw domain_error exceptions
        Vector operator / (const double scalar) const;
        Vector& operator /= (const double scalar);

        Vector operator ^ (const Vector &otherVector) const;

        bool operator == (const Vector &otherVector) const;
        bool operator != (const Vector &otherVector) const;

        /*Can throw out_of_range exceptions
            /!\ index is understood as a mathematical index, not as a general C++ index /!\
            See method 'at()' for the C++ like getter/setter*/
        double operator [] (const double index) const;
        double& operator [] (const double index);

        /*Can throw out_of_range exception
            C++ like getter/setter*/
        double at (const unsigned int index) const;
        double& at (const unsigned int index);

        friend std::ostream& operator << (std::ostream& os, const Vector& aVector);

        double dotProduct (const Vector &otherVector) const;    //Calculate Euclidea standard product


        //Other methods
        void addComponent (double value = 0);
        double magnitude () const;
        double magnitude2 () const;
        void show () const;


    private:
        std::vector<double> m_components;
};

#endif // VECTOR_H
