#ifndef SQMATRIX3_H
#define SQMATRIX3_H

#include <iostream>
#include <iomanip>
#include <cmath>
#include <vector>
#include <exception>
#include <stdexcept>

#include "vector3.h"

class SqMatrix3;
SqMatrix3 operator* (double scalar, const SqMatrix3 &other);
std::ostream& operator<< (std::ostream &out, const SqMatrix3 &matrix);

class SqMatrix3
{
    public:

	enum MatType
	{
	    Mat_Null, // the 0 matrix
	    Mat_Identity
	};

	SqMatrix3 (MatType type = Mat_Identity); // the identity I3 or the null matrix
	SqMatrix3 (const SqMatrix3 &other);
	SqMatrix3 (const Vector3 &col1, const Vector3 &col2, const Vector3 &col3);
	virtual ~SqMatrix3 ();

	SqMatrix3& operator= (const SqMatrix3 &other);

	// addition/substraction operators
	SqMatrix3 operator+ (const SqMatrix3 &other) const;
	SqMatrix3& operator+= (const SqMatrix3 &other);
	SqMatrix3 operator- (const SqMatrix3 &other) const;
	SqMatrix3& operator-= (const SqMatrix3 &other);

	// multiplication operators
	SqMatrix3 operator* (const SqMatrix3 &other) const;
	SqMatrix3& operator*= (const SqMatrix3 &other);
	SqMatrix3 operator* (double scalar) const;
	SqMatrix3& operator*= (double scalar);
	friend SqMatrix3 operator* (double scalar, const SqMatrix3 &other);
	Vector3 operator* (const Vector3& vector) const;

	// comparison operator
	bool operator== (const SqMatrix3 &other) const;
	bool operator!= (const SqMatrix3 &other) const;

	// transpose matrix
	SqMatrix3 transpose() const;
	SqMatrix3& setToTransposed();

	// determinant, inverse
	double determinant() const;
	double det() const; // this is an alias

	SqMatrix3 inverse() const;
	SqMatrix3& setToInverse();

	// index operators
	// math-like setter/getter
	double& operator() (unsigned int row, unsigned int col);
	double operator() (unsigned int row, unsigned int col) const;
	// C++ like setter/getter : indexes start at 0
	double& at (unsigned int row, unsigned int col);
	double at (unsigned int row, unsigned int col) const;

	// show matrix using cout by default
	void show(std::ostream &out = std::cout) const;

    private:
	void _setCols (const Vector3 *cols); // call only from another SqMatrix3 : cols is intended to be a 3-dim array

    protected:
	Vector3 *m_cols;
	static const unsigned int m_SIZE = 3; // size of the column vectors representing the matrix
};

#endif //SQMATRIX3_H

