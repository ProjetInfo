#ifndef VECTOR3_H
#define VECTOR3_H

#include <iostream>
#include <exception>
#include <stdexcept>
#include <cmath>

class Vector3
{
    public:
        enum STANDARD_BASIS {e1, e2, e3};       //Particular vector of R3, e1 = (1 0 0 ), e2 = (0 1 0), e3 = (0 0 1)
        enum DOT_PRODUCT_TYPE {EUCLID, W_EUCLID};   //Specify which scalar product is to be used (euclidian, weighted euclidian, etc).

        Vector3 ();
        Vector3 (const double cmp1, const double cmp2, const double cmp3);
        Vector3 (STANDARD_BASIS aVector);       //To instantiate a particular vector of R3
        Vector3 (double components[]);
        Vector3 (const Vector3 &otherVector);
        virtual ~Vector3 ();


        //Modifier method
        void setVector (const double cmp1 = 0, const double cmp2 = 0, const double cmp3 = 0);


        /*Operators
            All operators can throw logic_error exceptions*/
        void operator = (const Vector3 &otherVector);

        Vector3 operator + (const Vector3 &otherVector) const;
        Vector3& operator += (const Vector3 &otherVector);

        Vector3 operator - (const Vector3 &otherVector) const;
        Vector3& operator -= (const Vector3 &otherVector);

        Vector3 operator * (const double scalar) const;             //External product
        Vector3& operator *= (const double scalar);

        double operator * (const Vector3 &otherVector) const;       //Dot product of 2 vector3
        double operator *= (const Vector3 &otherVector) const;

        //Can throw domain_error exceptions
        Vector3 operator / (const double scalar) const;
        Vector3& operator /= (const double scalar);

        Vector3 operator ^ (const Vector3 &otherVector) const;      //Cross product of 2 vector3

        bool operator == (const Vector3 &otherVector) const;
        bool operator != (const Vector3 &otherVector) const;

        /*Can throw out_of_range exceptions
            /!\ index is understood as a mathematical index, not as a general C++ index /!\
            See method 'at()' for the C++ like getter/setter*/
        double operator [] (const unsigned int index) const;
        double& operator [] (const unsigned int index);

        /*Can throw out_of_range exception
            C++ like getter/setter*/
        double at (const unsigned int index) const;
        double& at (const unsigned int index);

        friend std::ostream& operator<< (std::ostream& os, const Vector3 &aVector);


        //Other methods
        double dotProduct (const Vector3 &otherVector, const DOT_PRODUCT_TYPE dpType = EUCLID,
                           const double w1 = 1, const double w2 = 1, const double w3 = 1) const;
        double magnitude () const;
        double magnitude2 () const;
        void show () const;

    private:
        double* m_components;
};

#endif // VECTOR3_H
