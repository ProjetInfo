#include "../inc/sqmatrix3.h"

SqMatrix3::SqMatrix3(MatType type)
{
    m_cols = new Vector3[m_SIZE];
    if (type != Mat_Null) {
	m_cols[0] = Vector3(Vector3::e1);
	m_cols[1] = Vector3(Vector3::e2);
	m_cols[2] = Vector3(Vector3::e3);
    }
}

SqMatrix3::SqMatrix3(const SqMatrix3 &other)
{
    m_cols = new Vector3[m_SIZE];
    _setCols(other.m_cols);
}

SqMatrix3::SqMatrix3 (const Vector3 &col1, const Vector3 &col2, const Vector3& col3)
{
    m_cols = new Vector3[m_SIZE];
    m_cols[0] = col1;
    m_cols[1] = col2;
    m_cols[2] = col3;
}

SqMatrix3::~SqMatrix3()
{
    delete[] m_cols;
    m_cols = 0;
}

// operator =
SqMatrix3& SqMatrix3::operator= (const SqMatrix3 &other)
{
    _setCols(other.m_cols);
    return (*this);
}

// arithmetic operators
// +, +=
SqMatrix3 SqMatrix3::operator+ (const SqMatrix3 &other) const
{
   return SqMatrix3(*this) += other;
}

SqMatrix3& SqMatrix3::operator+= (const SqMatrix3 &other)
{
    for (unsigned int i(0) ; i < m_SIZE ; ++i) {
	m_cols[i] += other.m_cols[i];
    }
    return (*this);
}

// -, -=
SqMatrix3 SqMatrix3::operator- (const SqMatrix3 &other) const
{
    return SqMatrix3(*this) -= other;
}

SqMatrix3& SqMatrix3::operator-= (const SqMatrix3 &other)
{
    for (unsigned int i(0) ; i < m_SIZE ; ++i) {
	m_cols[i] -= other.m_cols[i];
    }
    return (*this);
}

// multiplications
// matrix multiplication

#ifdef M_USE_STRASSEN
SqMatrix3 SqMatrix3::operator* (const SqMatrix3 &other) const
{

}
#else // use naive algorithm
SqMatrix3 SqMatrix3::operator* (const SqMatrix3 &other) const
{
    SqMatrix3 result(Mat_Null);

    for (unsigned int i(0) ; i < m_SIZE ; ++i) {
	for (unsigned int j(0) ; j < m_SIZE ; ++j) {
	    for (unsigned int k(0) ; k < m_SIZE ; ++k) {
		result.at(i,j) += this->at(i,k) * other.at(k,j);
	    }
	}
    }

    return result;
}
#endif // M_USE_STRASSEN

SqMatrix3& SqMatrix3::operator*= (const SqMatrix3 &other)
{
    return (*this = *this * other);
}

// scalar multiplication
SqMatrix3 SqMatrix3::operator* (double scalar) const
{
    return SqMatrix3(*this) *= scalar;
}

SqMatrix3& SqMatrix3::operator*= (double scalar)
{
    for (unsigned int i(0) ; i < m_SIZE ; ++i) {
	m_cols[i] *= scalar;
    }
    return (*this);
}

// matrix * vector3
Vector3 SqMatrix3::operator* (const Vector3 &vector) const
{
    // line vectors representing *this
    Vector3 *m_rows = new Vector3[m_SIZE];
    for (unsigned int i(0) ; i < m_SIZE ; ++i) {
	m_rows[i] = Vector3(this->at(i,0), this->at(i,1), this->at(i,2));
    }

    // result is a vector3
    Vector3 result;
    for (unsigned int i(0) ; i < m_SIZE ; ++i) {
	result.at(i) = m_rows[i].dotProduct(vector, Vector3::EUCLID); // inner product
    }

    delete[] m_rows;
    m_rows = 0;

    return result;
}

// comparison operator
bool SqMatrix3::operator== (const SqMatrix3 &other) const
{
    for (unsigned int i(0) ; i < m_SIZE ; ++i) {
	if (m_cols[i] != other.m_cols[i]) {
	    return false;
	}
    }
    return true;
}

bool SqMatrix3::operator!= (const SqMatrix3 &other) const
{
    return !(*this == other);
}

// transposition
SqMatrix3 SqMatrix3::transpose() const
{
    SqMatrix3 result(Mat_Null);
    for (unsigned int i(0) ; i < m_SIZE ; ++i) {
	result.m_cols[i] = Vector3(this->at(i,0), this->at(i,1), this->at(i,2));
    }
    return result;
}

SqMatrix3& SqMatrix3::setToTransposed()
{
    return ((*this) = this->transpose());
}

// determinant (alias function det points directly to determinant, could be useful if we are lazy)
double SqMatrix3::determinant() const
{
    return (((this->at(0,0)) * ((this->at(1,1))*this->at(2,2) - (this->at(1,2))*(this->at(2,1))))
     - ((this->at(0,1)) * ((this->at(1,0))*this->at(2,2) - (this->at(1,2))*(this->at(2,0))))
     + ((this->at(0,2)) * ((this->at(1,0))*this->at(2,1) - (this->at(1,1))*(this->at(2,0)))));
}

double SqMatrix3::det() const
{
    return this->determinant();
}

// inverse (direct formula)
SqMatrix3 SqMatrix3::inverse() const
{
    double det(this->determinant());
    if (det == 0.0) {
	throw std::domain_error("Tried to inverse matrix with null determinant");
    }

    Vector3 col1((this->at(1,1))*(this->at(2,2)) - (this->at(1,2))*(this->at(2,1)),
	    (this->at(1,2))*(this->at(2,0)) - (this->at(1,0))*(this->at(2,2)),
	    (this->at(1,0))*(this->at(2,1)) - (this->at(1,1))*(this->at(2,0)));
    Vector3 col2((this->at(0,2))*(this->at(2,1)) - (this->at(0,1))*(this->at(2,2)),
	    (this->at(0,0))*(this->at(2,2)) - (this->at(0,2))*(this->at(2,0)),
	    (this->at(0,1))*(this->at(2,0)) - (this->at(0,0))*(this->at(2,1)));
    Vector3 col3((this->at(0,1))*(this->at(1,2)) - (this->at(0,2))*(this->at(1,1)),
	    (this->at(0,2))*(this->at(1,0)) - (this->at(0,0))*(this->at(1,2)),
	    (this->at(0,0))*(this->at(1,1)) - (this->at(0,1))*(this->at(1,0)));

    SqMatrix3 result(col1, col2, col3);
    return (result * (1/det));
}

SqMatrix3& SqMatrix3::setToInverse()
{
    return (*this = this->inverse());
}

// index operators : indexes are common math indexes, not C++ array indexes
// setter (math-like)
double& SqMatrix3::operator() (unsigned int row, unsigned int col)
{
    if ((row > m_SIZE) or (col > m_SIZE)) {
	throw std::out_of_range("Passed ref index > 3 on 3 by 3 matrix");
    }
    else if ((row <= 0) or (col <= 0)) {
	throw std::out_of_range("Passed ref index 0 to SqMatrix3::operator()");
    }

    return m_cols[col-1][row];
}

// getter (math-like)
double SqMatrix3::operator() (unsigned int row, unsigned int col) const
{
    if ((row > m_SIZE) or (col > m_SIZE)) {
	throw std::out_of_range("Passed ref index > 3 on 3 by 3 matrix");
    }
    else if ((row <= 0) or (col <= 0)) {
	throw std::out_of_range("Passed ref index 0 to SqMatrix::operator()");
    }

    return m_cols[col-1][row];
}

//setter (C++-like)
double& SqMatrix3::at (unsigned int row, unsigned int col)
{
    if ((row >= m_SIZE) or (col >= m_SIZE)) {
	throw std::out_of_range("Passed ref index > 3 on 3 by 3 matrix");
    }

    return m_cols[col].at(row);
}

// getter (C++-like)
double SqMatrix3::at (unsigned int row, unsigned int col) const
{
    if ((row >= m_SIZE) or (col >= m_SIZE)) {
	throw std::out_of_range("Passed ref index > 3 on 3 by 3 matrix");
    }

    return m_cols[col].at(row);
}

// show matrix using cout by default
void SqMatrix3::show(std::ostream &out) const
{
    for (unsigned int i(0) ; i < m_SIZE ; ++i) {
	for (unsigned int j(0) ; j < m_SIZE ; ++j) {
	    out << std::setfill(' ') << std::setw(6) << at(i,j) << " ";
	}
	out << std::endl;
    }
}

// private methods
// _setCols : copy operator for attribute m_cols
void SqMatrix3::_setCols (const Vector3 *cols) // cols is intended to be a 3-dim array
{
    for (unsigned int i(0) ; i < m_SIZE ; ++i) {
	m_cols[i] = cols[i];
    }
}

// friend functions
// external product (scalar product)
SqMatrix3 operator* (double scalar, const SqMatrix3 &matrix)
{
    return matrix * scalar;
}

// operator<<
std::ostream& operator<< (std::ostream &out, const SqMatrix3 &matrix)
{
    matrix.show(out);
    return out;
}

