#include "../inc/vector.h"

using namespace std;

Vector::Vector () : m_components ()
{
}

Vector::Vector (const unsigned int dimension) : m_components (dimension, 0)
{
}

Vector::Vector (const double cmp1, const double cmp2, const double cmp3) : m_components(3)
{
    m_components.at(0) = cmp1;
    m_components.at(1) = cmp2;
    m_components.at(2) = cmp3;
}

Vector::Vector (double components[], const unsigned int size) : m_components (size)
{
    for (unsigned int i(0) ; i < size ; ++i) {
        m_components.at(i) = components[i];
    }
}

Vector::Vector (std::vector<double> components) : m_components (components)
{
}

Vector::Vector (const unsigned int dimension, STANDARD_BASIS aVector, const unsigned int i) : m_components (dimension, 0)
{
    if (i > dimension or i == 0) {
        throw logic_error ("Vector constructor : cannot instantiate vector !");
    }
    switch (aVector) {
        case ei:
            m_components.at(i-1) = 1;
            break;
    }
}

Vector::Vector (const Vector &otherVector) : m_components (otherVector.m_components)
{
}

//===================================================================

unsigned int Vector::dimension () const
{
    return m_components.size();
}

//===================================================================

//Operator =
void Vector::operator = (const Vector &otherVector)
{
    m_components = otherVector.m_components;
}

//Arithmetic operators +, +=
Vector Vector::operator + (const Vector &otherVector) const
{
    return ( Vector(*this) += otherVector );
}

Vector& Vector::operator += (const Vector &otherVector)
{
    const unsigned int SIZE (m_components.size());

    if (SIZE != otherVector.m_components.size()) {
        throw logic_error ("Vectors do not have the same dimension !");
    }

    for (unsigned int i(0) ; i < SIZE ; ++i) {
        m_components.at(i) += otherVector.m_components.at(i);
    }

    return (*this);
}

//Arithmetic operators -, -=
Vector Vector::operator - (const Vector &otherVector) const
{
    return ( Vector(*this) -= otherVector );
}

Vector& Vector::operator -= (const Vector &otherVector)
{
    const unsigned int SIZE (m_components.size());

    if (SIZE != otherVector.m_components.size()) {
        throw logic_error ("Vectors do not have the same dimension !");
    }

    for (unsigned int i(0) ; i < SIZE ; ++i) {
        m_components.at(i) -= otherVector.m_components.at(i);
    }

    return (*this);
}

//Multiplication by a scalar operators *, *=
Vector Vector::operator * (const double scalar) const
{
    return ( Vector(*this) *= scalar );
}

Vector& Vector::operator *= (const double scalar)
{
    const unsigned int SIZE (m_components.size());

    for (unsigned int i(0) ; i < SIZE ; ++i) {
        m_components.at(i) *= scalar;
    }
    return *this;
}

//Inner product operator
double Vector::operator * (const Vector &otherVector) const
{
    return (this->dotProduct(otherVector));
}

Vector Vector::operator / (const double scalar) const
{
    return ( Vector(*this) /= scalar );
}

Vector& Vector::operator /= (const double scalar)
{
    if (scalar == 0) {
        throw domain_error ("Vector::operator /= : Division by zero occured !");
    }

    const unsigned int SIZE (m_components.size());

    for (unsigned int i(0) ; i < SIZE ; ++i) {
        m_components.at(i) /= scalar;
    }
    return *this;
}

/*Cross product operator : /!\ only works for vector in dimension 3 /!\*/
Vector Vector::operator ^ (const Vector &otherVector) const
{
    if ( m_components.size() != 3 ) {
        throw logic_error ("Cross product only defined in dimension 3");
    }
    else if ( otherVector.dimension() != 3 ) {
        throw logic_error ("Cross product only defined in dimension 3");
    }

    Vector result;

    result.addComponent ( (m_components.at(1) * otherVector.m_components.at(2)) - ((m_components.at(2) * otherVector.m_components.at(1))) );
    result.addComponent ( (m_components.at(2) * otherVector.m_components.at(0)) - ((m_components.at(0) * otherVector.m_components.at(2))) );
    result.addComponent ( (m_components.at(0) * otherVector.m_components.at(1)) - ((m_components.at(1) * otherVector.m_components.at(0))) );
    return result;

}

//Equality operators ==, !=
bool Vector::operator == (const Vector &otherVector) const
{
    return (m_components == otherVector.m_components);
}

bool Vector::operator != (const Vector &otherVector) const
{
    return not(m_components == otherVector.m_components);
}

/*Index operators [] read-only
    /!\ index is understood as a mathematical index, not as a general C++ index /!\
                See method 'at()' for the C++ like getter
*/
double Vector::operator [] (const double index) const
{
    if (index > m_components.size() or index == 0) {
        throw out_of_range ("Vector::operator [] : Index is out of range !");
    }
    return ( m_components.at(index-1) );
}

/*Index operators [] read-only
    /!\ index is understood as a mathematical index, not as a general C++ index /!\
                See method 'at()' for the C++ like getter
*/
double& Vector::operator [] (const double index)
{
    if (index > m_components.size() or index == 0) {
        throw out_of_range ("Vector::operator [] : Index is out of range !");
    }
    return ( m_components.at(index-1) );
}

ostream& operator << (ostream& os, const Vector& aVector)
{
    const unsigned int SIZE (aVector.dimension());

    for (unsigned int i(0) ; i < SIZE ; ++i) {
        os << aVector.at(i) << ' ';
    }
    return os;
}

//===================================================================

/*Can throw out_of_range exception
    C++ like getter*/
double Vector::at (const unsigned int index) const
{
    if (index >= m_components.size()) {
        throw out_of_range ("Vector::at () : Index is out of range !");
    }
    return ( m_components.at(index) );
}

double& Vector::at (const unsigned int index)
{
    if (index >= m_components.size() ) {
        throw out_of_range ("Vector::at() : Index is out of range !");
    }
    return ( m_components.at(index) );
}

double Vector::dotProduct (const Vector &otherVector) const
{
    double result(0);

    const unsigned int SIZE(m_components.size());

    if ( SIZE == otherVector.components().size()) {
        for (unsigned int i(0) ; i < SIZE ; ++i) {
            result += ( m_components.at(i) * otherVector.components().at(i) );
        }
    }
    else {
        throw logic_error ("Vectors do not have the same dimension !");
    }

    return result;
}

//===================================================================

void Vector::addComponent (double value)
{
    m_components.push_back (value);
}

double Vector::magnitude () const
{
    return sqrt ( dotProduct(*this) );
}

double Vector::magnitude2 () const
{
    return ( dotProduct(*this) );
}

void Vector::show () const
{
    const unsigned int SIZE(m_components.size());

    for (unsigned int i(0) ; i < SIZE ; ++i) {
        cout << m_components.at(i) << endl;
    }
    cout << endl;
}
