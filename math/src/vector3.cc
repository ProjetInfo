#include "../inc/vector3.h"

using namespace std;

Vector3::Vector3 ()
{
    m_components = new double[3];
    m_components[0] = 0;
    m_components[1] = 0;
    m_components[2] = 0;
}

Vector3::Vector3 (const double cmp1, const double cmp2, const double cmp3)
{
    m_components = new double[3];
    m_components[0] = cmp1;
    m_components[1] = cmp2;
    m_components[2] = cmp3;
}

//Vector3 constructor for praticular vectors of R3
Vector3::Vector3 (STANDARD_BASIS aVector)
{
    m_components = new double[3];

    switch (aVector) {
        case e1:
            m_components[0] = 1;
            m_components[1] = 0;
            m_components[2] = 0;
            break;
        case e2:
            m_components[0] = 0;
            m_components[1] = 1;
            m_components[2] = 0;
            break;
        case e3:
            m_components[0] = 0;
            m_components[1] = 0;
            m_components[2] = 1;
            break;
        default:
            m_components[0] = 0;
            m_components[1] = 0;
            m_components[2] = 0;
            break;
    }
}

Vector3::Vector3 (double components[])
{
    m_components = new double[3];
    m_components[0] = components[0];
    m_components[1] = components[1];
    m_components[2] = components[2];
}

//Copy-constructor
Vector3::Vector3 (const Vector3 &otherVector)
{
    m_components = new double[3];
    m_components[0] = otherVector.m_components[0];
    m_components[1] = otherVector.m_components[1];
    m_components[2] = otherVector.m_components[2];
}

Vector3::~Vector3 ()
{
    delete[] m_components;
}

//===================================================================

void Vector3::setVector (const double cmp1, const double cmp2, const double cmp3)
{
    m_components[0] = cmp1;
    m_components[1] = cmp2;
    m_components[2] = cmp3;
}

//===================================================================

void Vector3::show () const
{
    cout << m_components[0] << endl;
    cout << m_components[1] << endl;
    cout << m_components[2] << endl << endl;
}

//===================================================================

//Operator =
void Vector3::operator = (const Vector3 &otherVector)
{
    m_components[0] = otherVector.m_components[0];
    m_components[1] = otherVector.m_components[1];
    m_components[2] = otherVector.m_components[2];
}

//Arithmetic operators +, +=
Vector3 Vector3::Vector3::operator + (const Vector3 &otherVector) const
{
    return ( Vector3(*this) += otherVector);
}

Vector3& Vector3::operator += (const Vector3 &otherVector)
{
    m_components[0] += otherVector.m_components[0];
    m_components[1] += otherVector.m_components[1];
    m_components[2] += otherVector.m_components[2];
    return *this;
}

//Arithmetic operators -, -=
Vector3 Vector3::operator - (const Vector3 &otherVector) const
{
    return ( Vector3(*this) -= otherVector );
}

Vector3& Vector3::operator -= (const Vector3 &otherVector)
{
    m_components[0] -= otherVector.m_components[0];
    m_components[1] -= otherVector.m_components[1];
    m_components[2] -= otherVector.m_components[2];
    return *this;
}

//Multiplication by a real number (double) operators
Vector3 Vector3::operator * (const double scalar) const
{
    return ( Vector3(*this) *= scalar );
}

Vector3& Vector3::operator *= (const double scalar)
{
    m_components[0] *= scalar;
    m_components[1] *= scalar;
    m_components[2] *= scalar;
    return *this;
}

//Inner product operators *, *=
double Vector3::operator * (const Vector3 &otherVector) const
{
    return ( dotProduct(otherVector) );
}

double Vector3::operator *= (const Vector3 &otherVector) const
{
    return ( dotProduct(otherVector) );
}

//Division by a real number operators /, /=
Vector3 Vector3::operator / (const double scalar) const
{
    if (scalar == 0) {
        throw domain_error ("Division by zero occured !");
    }
    return ( Vector3(*this) /= scalar );
}

Vector3& Vector3::operator /= (const double scalar)
{
    if (scalar == 0) {
        throw domain_error ("Division by zero occured !");
    }

    m_components[0] /= scalar;
    m_components[1] /= scalar;
    m_components[2] /= scalar;
    return *this;
}

//Cross product operator ^
Vector3 Vector3::operator ^ (const Vector3 &otherVector) const
{
    return ( Vector3( (m_components[1]*otherVector.m_components[2] - m_components[2]*otherVector.m_components[1]),
                     (m_components[2]*otherVector.m_components[0] - m_components[0]*otherVector.m_components[2]),
                     (m_components[0]*otherVector.m_components[1] - m_components[1]*otherVector.m_components[0]) ) );
}


//Equality operators ==, !=
bool Vector3::operator == (const Vector3 &otherVector) const
{
    return ( (m_components[0] == otherVector.m_components[0]) and
                (m_components[1] == otherVector.m_components[1]) and
                (m_components[2] == otherVector.m_components[2]) );

}

bool Vector3::operator != (const Vector3 &otherVector) const
{
    return not(*this == otherVector);
}

//Index operator [] read-only
// math like getter
double Vector3::operator [] (const unsigned int index) const
{
    if (index > 3 or index == 0)
    {
        throw out_of_range ("Vector3::operator[] : index is out of range !");
    }
    return ( m_components[(index - 1)]);
}

//Index operator [] with write possibility
// math like setter
double& Vector3::operator [] (const unsigned int index)
{
    if (index > 3 or index == 0)
    {
        throw out_of_range ("Vector3::operator[] : index is out of range !");
    }
    return ( m_components[(index - 1)]);
}

// in c++ like accessors/manipulators, case index == 0 shouldn't happen but the test is there for security...

// C++ like getter
double Vector3::at (const unsigned int index) const
{
    if (index > 2) {
	throw out_of_range ("Passed invalid C++ like index to Vector3::at");
    }
    return m_components[index];
}

// C++ like setter
double& Vector3::at (const unsigned int index)
{
    if (index > 2) {
	throw out_of_range ("Passed invalid C++ like index to Vector3::at");
    }
    return m_components[index];
}

//Overload of operator<< to use with every ostream objects (cout, files, etc.)
ostream& operator<< (ostream& os, const Vector3 &aVector)
{
    os << aVector.at(0) << ' ' << aVector.at(1) << ' ' << aVector.at(2);
    return os;
}

//===================================================================

//Inner product calculation method (used by operator * and *=)
double Vector3::dotProduct (const Vector3 &otherVector, const DOT_PRODUCT_TYPE dpType,
                            const double w1, const double w2, const double w3) const
{
    if (dpType == EUCLID) {         //Standard Euclidean inner product
        return ( (m_components[0]*otherVector.m_components[0]) +
                (m_components[1]*otherVector.m_components[1]) +
                (m_components[2]*otherVector.m_components[2]) );
    }
    else {          //Second method calculates weighted Euclidean inner product with weight w1, w2 and w3
        return ( w1*(m_components[0]*otherVector.m_components[0]) +
        w2*(m_components[1]*otherVector.m_components[1]) +
        w3*(m_components[2]*otherVector.m_components[2]) );
    }
}

//Magnitude calculation methods
double Vector3::magnitude () const
{
    return ( sqrt((*this) * (*this)) );
}

double Vector3::magnitude2 () const
{
    return ( (*this) * (*this) );
}
