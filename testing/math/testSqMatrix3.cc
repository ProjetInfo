#include <iostream>
#include "../../math/inc/sqmatrix3.h"

using namespace std;

int main()
{
    cout << "=== Testing SqMatrix3 ===" << endl;
    cout << "Test 1: creating three matrices: " << endl;

    // mat1 : identity in Mat3
    cout << "Mat1: must be identity" << endl;
    SqMatrix3 mat1;
    mat1.show();
    cout << endl;

    // mat2: null matrix in Mat3
    cout << "Mat2: must be null" << endl;
    SqMatrix3 mat2(SqMatrix3::Mat_Null);
    mat2.show();
    cout << endl;

    // mat3: [[1,2,3][4,5,6][7,8,9]]
    cout << "Mat3: must be [[1,2,3][4,5,6][7,8,9]] (row notation)" << endl;
    SqMatrix3 mat3(Vector3(1,4,7), Vector3(2,5,8), Vector3(3,6,9));
    mat3.show();
    cout << endl;

    cout << "=>" <<  " End of test 1" << endl << endl;

    //test 2 : operator<<
    cout << "Test 2: operator<<" << endl;
    cout << "Showing mat1 again: must be identity" << endl;
    cout << mat1 << endl;

    cout << "=>" << " End of test 2" << endl << endl;

    // test 3 : copy constructor and operator=
    cout << "Test 3: copy constructor and operator= " << endl;
    cout << "Copy constructor: creating Mat4 by copying Mat3" << endl;
    SqMatrix3 mat4(mat3);
    cout << "Showing mat4, must be [[1,2,3][4,5,6][7,8,9]]" << endl;
    cout << mat4 << endl;
    cout << "operator=: Mat1 = Mat4" << endl;
    mat1 = mat4;
    cout << "Showing Mat1: must be [[1,2,3][4,5,6][7,8,9]]" << endl;
    cout << mat1 << endl;
    cout << "=>" << " End of test 3" << endl << endl;

    // test 4 : sums, substractions
    cout << "Test 4: additions, substractions" << endl;
    cout << "Showing Mat1 + Mat2: must be [[1,2,3][4,5,6][7,8,9]]" << endl;
    cout << mat1 + mat2 << endl;

    cout << "Creating Mat5 := [[4.5,3,2.7][1,2,5.9][3,5.23,5]] " << endl;
    SqMatrix3 mat5(Vector3(4.5,1.0,3.0), Vector3(3.0,2.0,5.23), Vector3(2.7,5.9,5.0));
    cout << "Assigning Mat2 = Mat1 + Mat5" << endl;
    mat2 = mat1 + mat5;
    cout << "Showing mat2: must be [[5.5,5,5.7][5,7,11.9][10,13.23,14]]" << endl;
    cout << mat2 << endl;

    cout << "Creating Mat6 := null matrix" << endl;
    SqMatrix3 mat6(SqMatrix3::Mat_Null);
    cout << "Assigning Mat6 += Mat1" << endl;
    mat6 += mat1;
    cout << "Showing Mat6: must be [[1,2,3][4,5,6][7,8,9]]" << endl;
    cout << mat6 << endl;

    cout << "Assingning Mat6 += Mat5" << endl;
    mat6 += mat5;
    cout << "Showing Mat6: must be [[5.5,5,5.7][5,7,11.9][10,13.23,14]]" << endl;
    cout << mat6 << endl;

    cout << "Creating Mat7 := identity" << endl;
    SqMatrix3 mat7;
    cout << "Showing Mat1 - Mat7: must be [[0,2,3][4,4,6][7,8,8]]" << endl;
    cout << mat1 - mat7 << endl;

    cout << "Assigning Mat7 -= Mat2" << endl;
    mat7 -= mat2;
    cout << "Showing Mat7: must be [[-4.5,-5,-5.7][-5,-7,-11.9][-10,-13.23,-13]]" << endl;
    cout << mat7 << endl;

    cout << "=> End of test 4" << endl << endl;

    // test 5: comparison operators
    cout << "Test 5 : comparison operators" << endl;
    cout << "Testing Mat6 == Mat2: must be true ->: " << boolalpha << (mat6 == mat2) << endl;
    cout << "Testing Mat2 == Mat6: must be true ->: " << boolalpha << (mat2 == mat6) << endl;
    cout << "Testing Mat1 == Mat7: must be false ->: " << boolalpha << (mat1 == mat7) << endl;
    cout << "Testing Mat1 != Mat7: must be true ->: " << boolalpha << (mat1 != mat7) << endl;
    cout << "Testing Mat7 != Mat1: must ne true ->: " << boolalpha << (mat7 != mat1) << endl;

    cout << endl << "=> End of test 5" << endl << endl;

    // test 6: multiplication operators
    cout << "Test 6 : multiplication operators" << endl;
    cout << "Creating Mat8: identity" << endl;
    SqMatrix3 mat8;
    cout << "Showing Mat4 * Mat8: must be [[1,2,3][4,5,6][7,8,9]]" << endl;
    cout << mat4 * mat8 << endl;
    cout << "Showing Mat8 * Mat4: must be [[1,2,3][4,5,6][7,8,9]]" << endl;
    cout << mat8 * mat4 << endl;

    cout << "Showing Mat3 * Mat4: must be [[30,36,42][66,81,96][102,126,150]]" << endl;
    cout << mat3 * mat4 << endl;

    cout << "Assigning Mat3 *= Mat7" << endl;
    mat3 *= mat7;
    cout << "Showing Mat3: must be [[-44.5,-56,69,-68.5][-103,-129.38,-160.3][-161.5,-202.07,-252.1]]" << endl;
    cout << mat3 << endl;

    cout << "Creating Vec1 := [[58][59][60]]" << endl;
    Vector3 vec1(58,59,60);
    cout << "Showing Mat1 * Vec1: must be [[356][887][1418]]" << endl;
    (mat1 * vec1).show();
    cout << endl;

    cout << "Showing 2 * Mat1: must be [[2,4,6][8,10,12][14,16,18]]" << endl;
    cout << (2 * mat1) << endl;
    cout << "Showing Mat1 * 2: must be [[2,4,6][8,10,12][14,16,18]]" << endl;
    cout << (mat1 * 2) << endl;
    cout << "Assigning Mat1 *= 2" << endl;
    mat1 *= 2;
    cout << "Showing Mat1: must be [[2,4,6][8,10,12][14,16,18]]" << endl;
    cout << mat1 << endl;

    cout << "=> End of test 6" << endl << endl;

    // test 7: transposition
    cout << "Test 7: Transposition" << endl;
    cout << "Showing T(Mat4): must be [[1,4,7][2,5,8][3,6,9]]" << endl;
    cout << mat4.transpose() << endl;
    cout << "Assigning Mat4 = T(Mat4)" << endl;
    mat4.setToTransposed();
    cout << "Showing Mat4: must be [[1,4,7][2,5,8][3,6,9]]" << endl;
    cout << mat4 << endl;

    cout << "=> End of test 7" << endl;

    // test 8: determinant, inverse
    cout << "Test 8: determinant, inverse" << endl;
    cout << "det(Mat4): must be 0 ->: " << mat4.determinant() << endl;
    cout << "det(Mat7): must be 52,4115 ->: " << mat7.determinant() << endl;

    cout << "Trying to inverse Mat4: must throw std::domain_error exception" << endl;
    try {
	mat4.inverse();
    }
    catch (domain_error &err) {
	cout << err.what() << endl;
    }
    cout << "End of catch block" << endl;
    cout << "Showing Mat7 inverse: must be [[-1.51564,0.19864,0.482718][1.03031,0.0286197,-0.477949][-0.117341,-0.181926,0.0381596]]" << endl;
    cout << mat7.inverse() << endl;
    cout << "Assigning Mat7 = Mat7 inverse" << endl;
    mat7.setToInverse();
    cout << "Showing Mat7: must be [[-1.51564,0.19864,0.482718][1.03031,0.0286197,-0.477949][-0.117341,-0.181926,0.0381596]]" << endl;
    cout << mat7 << endl;

    cout << "=> End of test 8" << endl;

    // automatic tests : trust them only if tests 1 to 8 have been successful
    cout << "Automatic tests: should be trusted only if tests 1 to 8 have been successful" << endl;
    cout << "Getter/Setter tests:" << endl;
    cout << setfill(' ') << setw(5) << "Access: ";
    if (mat1(1,2) == 4.0) {
	cout << "SUCCESS" << endl;
    }
    else {
	cout << "FAIL" << endl;
    }
    cout << setfill(' ') << setw(5) << "Modify: ";
    mat1(1,1) = 5.76;
    mat1(1,1) == 5.76 ? cout << "SUCCESS" << endl : cout << "FAIL" << endl;
    cout << "Multiplication tests:" << endl;
    SqMatrix3 mat9(Vector3(1,4,7), Vector3(2,5,8), Vector3(3,6,10));
    cout << setfill(' ') << setw(5) <<  "Mat * Mat^(-1): ";
    if (mat8 * mat8.inverse() == SqMatrix3()) {
	cout << "SUCCESS" << endl;
    }
    else {
	cout << "FAIL" << endl;
    }

    cout << endl << endl << "End of SqMatrix3 test" << endl;
    return 0;
}

