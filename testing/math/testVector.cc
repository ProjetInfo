#include <iomanip>
#include "../../math/inc/vector.h"

using namespace std;

bool test (const Vector &testVal, const Vector &refVal) {return (testVal == refVal);}
bool test (const double testVal, const double refVal) {return (testVal == refVal);}

int main ()
{
    Vector vec1 (vector<double> (5,1));
    Vector vec2 (vector<double> (5, -2.25));
    Vector vec3 (4);
    Vector vec4 (vec3);
    Vector vec5 (1, -3.144, 5);
    double array[4] = {1, -2, 8, 3.21};
    Vector vec6 (array, 4);
    Vector e3 (8, Vector::ei, 3);


    cout << "**** Beginnning of preliminary testing ****" << endl << endl;

    cout << "Testing constructors. Output should be (1 1 1 1 1), \n(-2.25 -2.25 -2.25 -2.25 -2.25), 2x(0 0 0 0), e3 in R8" << endl;
    cout << "(1 -3.144 5) and (1 -2 8 3.21)" << endl << endl;
    cout << vec1 << endl;
    cout << vec2 << endl;
    cout << vec3 << endl;
    cout << vec4 << endl;
    cout << e3 << endl;
    cout << vec5 << endl;
    cout << vec6 << endl << endl;


    cout << "Testing overload of operator << (implementation only for testing purpose)" << endl << "Output should be (0 0 0 0)" << endl;
    cout << vec4 << endl;

    cout << "Testing operator = : Output should be (-2.25 -2.25 -2.25 -2.25-2.25)" << endl;
    vec3 = vec2;
    cout << vec3 << endl;
    vec3 = vec4;

    cout << "Testing operator == : output should be FALSE TRUE" << endl;
    cout << boolalpha << (vec1 == vec2) << '\t' << (vec3 == vec2) << endl << endl;

    cout << "**** End of preliminary testing ****" << endl
            << "/!\\ If any of the above tests failed, discard rest of tests /!\\" << endl << endl;

    cout << "**** Beginning of methods testing ****" << endl << endl;

    cout << "at() const test :    ";
    cout << boolalpha << ( test(vec2.at(2), -2.25) and test(e3.at(3), 0) ) << endl;

    cout << "at() non const test :    "; {
    Vector e4 (4, Vector::ei, 1);
    e4.at(0) = 1;
    e4.at(3) = 3.1415;
    cout << boolalpha << ( test(e4.at(0), 1) and test(e4.at(3), 3.1415) ) << endl;
    }

    cout << "'addComponent()' test :    "; {
    vec3.addComponent(4);
    Vector refVal (5);
    refVal.at(4) = 4;
    cout << boolalpha << test (vec3, refVal) << endl;
    }


    cout << "magnitude() test :    ";
    Vector mag (vector<double>(25, 5));
    cout << boolalpha << test(mag.magnitude(), 25) << endl;
    cout << "magnitude2() test :    ";
    cout << boolalpha << test (vec1.magnitude2(), 5) << endl;

    cout << "show() test : Output should be (0 0 1 0 0 0 0 0)" << endl;
    e3.show();

    cout << "**** End of methods testing ****" << endl << endl;

    cout << "**** Beginning of operators testing ****" << endl << endl;

    cout << "Operator + test :    "; {
    vec3 = vec1 + vec2;
    Vector refVal (vector<double> (5,-1.25));
    cout << boolalpha << test (vec3, refVal) << endl;
    }

    cout << "Operator += test :    "; {
    vec1 += vec3;
    Vector refVal (vector<double> (5,-0.25));
    cout << boolalpha << test (vec1, refVal) << endl;
    }

    cout << "Operator - test :    "; {
    vec3 = vec1 - vec2;
    Vector refVal (vector<double> (5,2));
    cout << boolalpha << test (vec3, refVal) << endl;
    }

    cout << "Operator -= test :    "; {
    vec2 -= vec3;
    Vector refVal (vector<double> (5,-4.25));
    cout << boolalpha << test (vec2, refVal) << endl;
    }

    cout << "Operator * (scalar) test :    "; {
    vec3 = vec1 * -3;
    Vector refVal (vector<double> (5,0.75));
    cout << boolalpha << test (vec3, refVal) << endl;
    }

    cout << "Operator *= (scalar) test :    "; {
    vec1 *= 4;
    Vector refVal (vector<double> (5,-1));
    cout << boolalpha << test (vec1, refVal) << endl;
    }

    cout << "Operator * (dotProduct) test :    "; {
    cout << boolalpha << test ((vec2 * vec3), -15.9375) << endl;
    }

    cout << "Operator / test :    "; {
    Vector a (vector<double> (5, -6));
    vec3 = a / -3;
    Vector refVal (vector<double> (5,2));
    cout << boolalpha << test (vec3, refVal) << endl;
    }

    cout << "Operator /= test :    "; {
    Vector a (vector<double> (5, 1.75));
    a /= 1.25;
    Vector refVal (vector<double> (5,1.4));
    cout << boolalpha << test (a, refVal) << endl;
    }

    cout << "Operator ^ test :    "; {
    Vector e2 (3, Vector::ei, 2);
    Vector a;
    a.addComponent(5);
    a.addComponent(3);
    a.addComponent(2.27);
    Vector refVal;
    refVal.addComponent(2.27);
    refVal.addComponent(0);
    refVal.addComponent(-5.0);
    cout << boolalpha << test ((e2^a), refVal) << endl;
    }

    cout << "Operator != test :    ";
    cout << boolalpha << ((vec1 != vec2) == true) << endl;

    cout << "Operator [] test :    "; {
    cout << boolalpha << ( test(vec1[5], -1) and test(vec3[2], 2) ) << endl;
    }

    cout << endl;
    cout << "**** End of testing operators ****" << endl << endl;

    cout << "**** Beginning of exception handling testing ****" << endl << endl;

    try {
        cout << "Operator +" << endl;
        vec1 = e3 + vec2;
    }
    catch (logic_error &e) {
        cout << e.what() << endl;
    }

    try {
        cout << "Operator +=" << endl;
        vec1 += e3;
    }
    catch (logic_error &e) {
        cout << e.what() << endl;
    }

    try {
        cout << "Operator -" << endl;
        vec1 = e3 - vec2;
    }
    catch (logic_error &e) {
        cout << e.what() << endl;
    }

    try {
        cout << "Operator -=" << endl;
        vec1 -= e3;
    }
    catch (logic_error &e) {
        cout << e.what() << endl;
    }

    try {
        cout << "Operator *" << endl;
        vec1 = e3 * vec2;
    }
    catch (logic_error &e) {
        cout << e.what() << endl;
    }

    try {
        cout << "Operator /" << endl;
        vec1 = e3 / 0;
    }
    catch (domain_error &e) {
        cout << e.what() << endl;
    }

    try {
        cout << "Operator /=" << endl;
        vec1 /= 0;
    }
    catch (domain_error &e) {
        cout << e.what() << endl;
    }

    try {
        cout << "Operator ^" << endl;
        vec3 = vec1 ^ vec2;
    }
    catch (logic_error &e) {
        cout << e.what() << endl;
    }

    try {
        cout << "at() const" << endl;
        vec1.at(100);
    }
    catch (out_of_range &e) {
        cout << e.what() << endl;
    }

    try {
        cout << "at() non-const" << endl;
        vec1.at(6) = 2;
    }
    catch (out_of_range &e) {
        cout << e.what() << endl;
    }

    try {
        cout << "[] const" << endl;
        vec1[100];
    }
    catch (out_of_range &e) {
        cout << e.what() << endl;
    }

    try {
        cout << "[] non-const" << endl;
        vec1[7] = 2;
    }
    catch (out_of_range &e) {
        cout << e.what() << endl;
    }

    cout << endl << endl << "**** End of testing ****" << endl << endl;

    return 0;
}


