#include <iostream>
#include "../../math/inc/vector3.h"

using namespace std;

int main ()
{
    double array[3] = {1, -2.36, 5.78};
    Vector3 vec1, vec2 (2.0, 2.0, 2.0), e1 (Vector3::e1), vec (array);

    cout << "Initialisation test :" << endl << "Output should be (0 0 0), (2 2 2), (1 0 0) and (1 -2.36 5.78)" << endl;

    vec1.show();
    vec2.show();
    e1.show();
    vec.show();

    cout << "Copy constructor test :" << endl << "Output should be (2 2 2)" << endl;
    Vector3 vec3 (vec2);
    vec3.show();


    cout << endl << "**** Beginning of methods testing ****" << endl << endl;

    Vector3 vec5;
    cout << "'setVector' test :" << endl;
    cout << "vec5.setVector(1), vec5.setVector(1.5 2.36), vec5.setVector(3.1415 -62.36 5)" << endl;
    cout << "Output should be (1 0 0), (1.5 2.36 0) and (3.1415 -62.36 5)" << endl;
    vec5.setVector(1);   vec5.show();
    vec5.setVector(1.5, 2.36);   vec5.show();
    vec5.setVector(3.1415, -62.36, 5);   vec5.show();


    cout << "Magnitude test :" << endl << "Output should be 3.4641 and 12" << endl;
    cout << vec2.magnitude() << '\t' << vec2.magnitude2() << endl;

    cout << "Dot product test :" << endl << "vec2 * e1 = 2 and vec2 * vec3 = 12" << endl;
    cout << vec2.dotProduct(e1) << '\t' << vec2.dotProduct(vec3) << endl;

    cout << endl << "**** End of methods testing ****" << endl << endl;
<<<<<<< HEAD

    cout << endl << "**** Beginning of operators testing ****" << endl <<endl;

    cout << "Operator 'ostream& operator<< ()' test : Output should be (1 2.5 -3.74)" << endl;
    cout << vec << endl << endl;
=======


    cout << endl << "**** Beginning of operators testing ****" << endl <<endl;

    cout << "Operator 'ostream operator<< ()' test : Output should be (3.1415 -62.36 5)" << endl;
    cout << vec5 << endl << endl;
>>>>>>> vector-Rn

    cout << "Operator += test : e1 += vec2 = (3 2 2)" << endl;
    e1 += vec2;
    e1.show();
    cout << "Operator -= test : e1 -= vec2 = (1 0 0)" << endl;
    e1 -= vec2;
    e1.show();

    cout << "Operator + test : vec1 = e1 + vec2 = (3 2 2)" << endl;
    vec1 = e1 + vec2;
    vec1.show();

    cout << "Operator - test : vec1 = e1 - vec2 = (-1 -2 -2)" << endl;
    vec1 = e1 - vec2;
    vec1.show();

    cout << "Operator *= 'scalar' test : vec1 *= -1.5 = (1.5 3 3)" << endl;
    vec1 *= -1.5;
    vec1.show();

    cout << "Operator * 'scalar' test : vec3 = vec1*2.5 = (3.75 7.5 7.5)" << endl;
    vec3 = vec1*2.5;
    vec3.show();

    cout << "Operator /= test : vec3 /= 5 = (0.75 1.5 1.5)" << endl;
    vec3 /= 5;
    vec3.show();

    cout << "Operator /= : attempting division by zero..." << endl;
    try {
    vec3 /= 0.0;
    }
    catch (domain_error& e) {
        cout << e.what() << endl << endl;
    }

    cout << "Operator / test : vec2 = vec1 / 0.75 = (2 4 4)" << endl;
    vec2 = vec1 / 0.75;
    vec2.show();

    cout << "Operator / : attempting division by zero..." << endl;
    try {
    vec2 = vec1 / 0.0;
    }
    catch (domain_error& e) {
        cout << e.what() << endl << endl;
    }

    cout << "Operator ^ test : vec1 = e1 x vec2 = (0 -4 4)" << endl;
    vec1 = e1 ^ vec2;
    vec1.show();

    cout << "Operator ^ test : vec1 = vec2 x e1 = (0 4 -4)" << endl;
    vec1 = vec2 ^ e1;
    vec1.show();

    cout << "Operator = test : vec1 = vec4 = (1 3 7)" << endl;
    Vector3 vec4 (1, 3, 7);
    vec1 = vec4;
    vec1.show();

    cout << "Operator == test : vec1 == vec4 -> TRUE and vec1 == vec2 -> FALSE" << endl;
    cout << boolalpha << (vec1 == vec4) << '\t' << (vec1 == vec2) << endl;

    cout << "Operator != test : vec1 != vec3 -> TRUE and vec1 != vec4 -> FALSE" << endl;
    cout << boolalpha << (vec1 != vec3) << '\t' << (vec1 != vec4) << endl << endl;

    cout << "Operator [] test :" << endl;
    cout << "vec1[2] : " << vec1[2] << "\t Should be 3" << endl;
    cout << "vec1[1] = 3 and vec1[3] = 3.1415. Output should be (3 3 3.1415)" << endl;
    vec1[1] = 3;
    vec1[3] = 3.1415;
    vec1.show();

    cout << "Operator [] : attempting vec1[4]" << endl;
    try {
    cout << vec1[4] << endl;
    }
    catch (out_of_range& e) {
    cout << e.what() << endl << endl;
    }

    cout << "Operator [] : attempting vec1[0]" << endl;
    try {
    cout << vec1[0] << endl;
    }
    catch (out_of_range& e) {
    cout << e.what() << endl << endl;
    }

    cout << endl << endl << "**** End of testing ! ****" << endl << endl;

    return 0;
}
